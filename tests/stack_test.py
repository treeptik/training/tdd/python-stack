import pytest
from stack import Stack


class TestStack:
    class TestGivenEmptyStack:
        @pytest.fixture()
        def stack(self):
            return Stack()

        def test_is_empty(self, stack):
            assert stack.is_empty()

        def test_pop_returns_none(self, stack):
            with pytest.raises(IndexError):
                stack.pop()

        class TestWhenPush:
            @pytest.fixture()
            def element(self):
                return 1

            @pytest.fixture()
            def stack(self, stack, element):
                stack.push(element)
                return stack

            def test_is_not_empty(self, stack):
                assert not stack.is_empty()

            def test_pop_returns_element(self, stack, element):
                assert stack.pop() == element

    class TestGivenNonEmptyStack:
        @pytest.fixture()
        def stack(self):
            stack = Stack()
            stack.push(1)
            stack.push(2)
            stack.push(3)
            return stack

        def test_is_not_empty(self, stack):
            assert not stack.is_empty()

        class TestWhenPush:
            @pytest.fixture()
            def stack(self, stack):
                stack.push(4)
                return stack

            def test_pop_returns_element(self, stack):
                assert stack.pop() == 4

            def test_retains_original_elements(self, stack):
                stack.pop()
                assert stack.pop() == 3
                assert stack.pop() == 2
                assert stack.pop() == 1

    class TestGivenSingletonStack:
        @pytest.fixture()
        def stack(self):
            stack = Stack()
            stack.push(1)
            return stack

        def test_is_not_empty(self, stack):
            assert not stack.is_empty()

        def test_pop_empties_stack(self, stack):
            stack.pop()
            assert stack.is_empty()
