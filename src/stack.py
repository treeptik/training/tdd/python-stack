class Stack:
    def __init__(self):
        self.contents = []

    def is_empty(self):
        return len(self.contents) == 0

    def push(self, x):
        self.contents.append(x)

    def pop(self):
        return self.contents.pop()
