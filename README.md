# TDD Python Stack

[![(c) Treeptik, Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

## License

See [License](LICENSE.md).

## Prerequisites

- Python 3.7
- Pipenv 2018.11.26

## From Scratch

```shell
mkdir -p path/to/project
cd path/to/project

pipenv --python 3.7
pipenv install --dev pytest~=4.6

echo "PYTHONPATH=src/" > .env
wget -qO.gitignore https://gitignore.io/api/git,python,pycharm,pycharm+all,vim,linux,macos,windows
echo "!.env" >> .gitignore
```

## Running Tests

```shell
pipenv run pytest
```
